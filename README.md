# Validata Ops

Resources to deploy Validata to Kubernetes

## Prerequisites

Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

Retrieve the kubeconfig YAML file from your Scaleway cluster and save it to disk.

Export as KUBECONFIG environment variable

```bash
export KUBECONFIG=/path/to/kubeconfig.yaml
```

## Cluster setup

### Load balancer for ingress controller

Follow:

- <https://www.scaleway.com/en/docs/using-a-load-balancer-to-expose-your-kubernetes-kapsule-ingress-controller-service/>

```bash
kubectl apply -k cluster-setup/ingress-load-balancer/
```

Then edit the DNS zone to let the wildcard domain point to the external IP of the load balancer.

### SSL certificates

Install cert-manager, following:

- <https://cert-manager.io/docs/installation/kubernetes/>

```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.yaml
```

Install cluster issuers:

```bash
kubectl apply -k cluster-setup/cert-manager/
```

## Deploy

To deploy a new Validata release to the Kubernetes cluster:

- Insure that app container images you want to deploy are available in GitLab container registry:
  - [validata-ui](https://gitlab.com/validata-table/validata-ui/container_registry/4)
  - [validata-api](https://gitlab.com/validata-table/validata-api/container_registry/3)
- update versions in yaml files:
  - validata-api: [validata-api-deployment-patch.yaml](k8s/overlays/go.validata.fr/validata-api-deployment-patch.yaml)
  - validata-ui: [validata-ui-deployment-patch.yaml](k8s/overlays/go.validata.fr/validata-ui-deployment-patch.yaml)
- update configuration if needed in [config-configmap.yaml](k8s/overlays/go.validata.fr/config-configmap.yaml)

- push changes to the cluster:

```bash
export KUBECONFIG=/path/to/kubeconfig.yaml

kubectl apply -k k8s/overlays/go.validata.fr/
```

- Follow deployment using [k9s](https://k9scli.io/)
